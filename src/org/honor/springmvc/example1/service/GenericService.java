package org.honor.springmvc.example1.service;

import java.util.List;

public interface GenericService {
	public List<String> getWelcomeMessage(String name);
}
