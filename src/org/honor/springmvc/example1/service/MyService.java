package org.honor.springmvc.example1.service;

import java.util.ArrayList;
import java.util.List;

public class MyService implements GenericService {

	@Override
	public List<String> getWelcomeMessage(String name) {
		List<String> myWelcomeMessage  = new ArrayList<>();
		
		myWelcomeMessage.add("Hello ");
		myWelcomeMessage.add(name);
		myWelcomeMessage.add("! Welcome to my example project...!");
		return myWelcomeMessage;
	}

}
